#### Sharkatzor News Notify

Notify a Discord channel in case a new post is available on Age of Empires news page.


#### License

[MIT](LICENSE)