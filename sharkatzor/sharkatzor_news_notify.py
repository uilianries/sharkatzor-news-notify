import requests
from bs4 import BeautifulSoup
import logging
import argparse
import configparser


LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)
S_HANDLER = logging.StreamHandler()
S_HANDLER.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
S_HANDLER.setFormatter(formatter)
LOGGER.addHandler(S_HANDLER)


class PostNews(object):
    def __init__(self, url="", title=""):
        self.url = url
        self.title = title
        self._config = configparser.ConfigParser()
        self._config["post"] = {"url": self.url, "title": self.title}

    def __eq__(self, other):
        return self.url == other.url

    def store(self, json_path):
        with open(json_path, 'w') as fd:
            self._config.write(fd)

    @staticmethod
    def generate(soup):
        # TODO: Parse A element to get URL and Title
        element = soup.find("a", class_="featured-news__post__more-button")
        url = element.get("href")
        element = soup.find("h2", class_="featured-news__post__title")
        title = element.get("span")
        return PostNews(url, title)

    @staticmethod
    def restore(json_path):
        try:
            config = configparser.ConfigParser()
            config.read(json_path)
            return PostNews(config["post"]["url"], config["post"]["title"])
        except Exception:
            return PostNews()


class NewsScrapper:

    def __init__(self, logger):
        self._logger = logger

    def get_latest_post(self, news_url: str) -> PostNews:
        html_response = requests.get(url=news_url)
        html_response.raise_for_status()
        soup = BeautifulSoup(html_response.content, "html.parser")
        element = soup.find("a", class_="featured-news__post__more-button")
        url = element.get("href")
        if url is None:
            raise Exception("Could not find URL for latest post")
        element = soup.find("h2", class_="featured-news__post__title")
        title = element.get_text()
        if title is None:
            raise Exception("Could not find title for latest post")
        return PostNews(url, title)


class DiscordMessenger:

    def __init__(self, logger, access_token, news_channel, maintenance_channel):
        self._logger = logger
        self._news_channel = news_channel
        self._maintenance_channel = maintenance_channel
        self._access_token = access_token

    def _send_message(self, channel, message):
        response = requests.post(
            f"https://discordapp.com/api/channels/{channel}/messages",
            headers={"Authorization": f"Bot {self._access_token}"},
            json={"content": message}
        )
        response.raise_for_status()
        json_data = response.json()
        return json_data['id']

    def send_news_message(self, message):
        self._logger.debug("Sending message to news channel on Discord: {}".format(message))
        self._send_message(self._news_channel, message)

    def send_maintenance_message(self, message):
        self._logger.debug("Sending message to maintenance channel on Discord: {}".format(message))
        self._send_message(self._maintenance_channel, message)


class SharkatzorConfiguration:

    def __init__(self, logger, config_file_path):
        self._logger = logger
        self._config = self._read_config(config_file_path)

    def _read_config(self, config_file_path):
        config = configparser.ConfigParser()
        config.read(config_file_path)
        return config

    def _log_configuration(self):
        self._logger.info(f'Website for news: {self._config["website"]["news_url"]}')
        self._logger.info('Discord Token: {}****'.format(self._config["discord"]["token"][:4]))
        self._logger.info('News Discord channel: {}****'.format(self._config["discord"]["news_channel"][:4]))
        self._logger.info('Maintenance Discord channel: {}****'.format(self._config["discord"]["maintenance_channel"][:4]))

    @property
    def configuration(self):
        return self._config


class SharkatzorNewsNotifier:

    def __init__(self, config_path):
        self._logger = LOGGER
        self._logger.info('Starting ...')
        self._config = self._load_configuration(config_path)

    def _load_configuration(self, config_path):
        shark_config = SharkatzorConfiguration(self._logger, config_path)
        return shark_config.configuration

    def run(self):
        news_scrapper = NewsScrapper(self._logger)
        latest_post = news_scrapper.get_latest_post(self._config["website"]["news_url"])
        self._logger.debug(f"Latest post on AoE news: {latest_post.title}")
        cache_post = PostNews.restore(self._config["store"]["path"])
        if cache_post != latest_post:
            self._logger.info(f"New post: ({latest_post.title}): '{latest_post.url}'")
            discord = DiscordMessenger(self._logger, self._config["discord"]["token"], self._config["discord"]["news_channel"], self._config["discord"]["maintenance_channel"])
            try:
                discord.send_news_message(f"Post novo no site do Age of Empires!\n**{latest_post.title}**\n{latest_post.url}")
                latest_post.store(self._config["store"]["path"])
            except Exception as err:
                self._logger.error(f"Could not send message to Discord: {err}")
                discord.send_maintenance_message(f"Caramba, não consegui mandar mensagem no Discord!:\n{err}")


def main():
    argparser = argparse.ArgumentParser(description="Sharkatzor News Notifier for Discord")
    argparser.add_argument("-c", "--config", help="Configuration file path", default="/etc/sharkatzor/conf.ini")

    args = argparser.parse_args()

    sharkatzor = SharkatzorNewsNotifier(args.config)
    sharkatzor.run()


if __name__ == "__main__":
    main()

